﻿
namespace Project20210209WF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Project20210209WF.MyDateInfo myDateInfo1 = new Project20210209WF.MyDateInfo();
            Project20210209WF.MyDateInfo myDateInfo2 = new Project20210209WF.MyDateInfo();
            this.clickMe = new System.Windows.Forms.Button();
            this.message = new System.Windows.Forms.Label();
            this.currentDateAndTime = new System.Windows.Forms.Label();
            this.currentDateAndTime2 = new System.Windows.Forms.Label();
            this.person = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.myNumericBox1 = new Project20210209WF.MyNumericBox();
            this.birthDay = new Project20210209WF.MyComposedControl();
            this.myComposedControl2 = new Project20210209WF.MyComposedControl();
            this.SuspendLayout();
            // 
            // clickMe
            // 
            this.clickMe.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clickMe.ForeColor = System.Drawing.Color.DarkRed;
            this.clickMe.Location = new System.Drawing.Point(215, 260);
            this.clickMe.Name = "clickMe";
            this.clickMe.Size = new System.Drawing.Size(148, 67);
            this.clickMe.TabIndex = 1;
            this.clickMe.Text = "Click me!";
            this.clickMe.UseVisualStyleBackColor = true;
            this.clickMe.Click += new System.EventHandler(this.clickMe_Click);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.BackColor = System.Drawing.SystemColors.Highlight;
            this.message.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.message.Location = new System.Drawing.Point(200, 363);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(103, 13);
            this.message.TabIndex = 2;
            this.message.Text = "Initial message here!";
            // 
            // currentDateAndTime
            // 
            this.currentDateAndTime.AutoSize = true;
            this.currentDateAndTime.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentDateAndTime.Location = new System.Drawing.Point(599, 32);
            this.currentDateAndTime.Name = "currentDateAndTime";
            this.currentDateAndTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.currentDateAndTime.Size = new System.Drawing.Size(189, 14);
            this.currentDateAndTime.TabIndex = 3;
            this.currentDateAndTime.Text = "Current Date and Time here";
            // 
            // currentDateAndTime2
            // 
            this.currentDateAndTime2.AutoSize = true;
            this.currentDateAndTime2.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentDateAndTime2.Location = new System.Drawing.Point(599, 81);
            this.currentDateAndTime2.Name = "currentDateAndTime2";
            this.currentDateAndTime2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.currentDateAndTime2.Size = new System.Drawing.Size(189, 14);
            this.currentDateAndTime2.TabIndex = 4;
            this.currentDateAndTime2.Text = "Current Date and Time here";
            // 
            // person
            // 
            this.person.Location = new System.Drawing.Point(432, 260);
            this.person.Name = "person";
            this.person.Size = new System.Drawing.Size(181, 20);
            this.person.TabIndex = 5;
            this.person.Text = "World";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(450, 320);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Hello, World!";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(634, 260);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // myNumericBox1
            // 
            this.myNumericBox1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.myNumericBox1.ForeColor = System.Drawing.Color.DarkRed;
            this.myNumericBox1.Location = new System.Drawing.Point(560, 312);
            this.myNumericBox1.Name = "myNumericBox1";
            this.myNumericBox1.Size = new System.Drawing.Size(100, 20);
            this.myNumericBox1.TabIndex = 8;
            this.myNumericBox1.Text = "19";
            this.myNumericBox1.Value = 19;
            // 
            // birthDay
            // 
            myDateInfo1.Day = 1;
            myDateInfo1.Month = 1;
            myDateInfo1.Year = 1;
            this.birthDay.DateValue = myDateInfo1;
            this.birthDay.Location = new System.Drawing.Point(36, 22);
            this.birthDay.Name = "birthDay";
            this.birthDay.Size = new System.Drawing.Size(501, 73);
            this.birthDay.TabIndex = 9;
            // 
            // myComposedControl2
            // 
            myDateInfo2.Day = 1;
            myDateInfo2.Month = 1;
            myDateInfo2.Year = 1;
            this.myComposedControl2.DateValue = myDateInfo2;
            this.myComposedControl2.Location = new System.Drawing.Point(36, 91);
            this.myComposedControl2.Name = "myComposedControl2";
            this.myComposedControl2.Size = new System.Drawing.Size(501, 73);
            this.myComposedControl2.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.myComposedControl2);
            this.Controls.Add(this.birthDay);
            this.Controls.Add(this.myNumericBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.person);
            this.Controls.Add(this.currentDateAndTime2);
            this.Controls.Add(this.currentDateAndTime);
            this.Controls.Add(this.message);
            this.Controls.Add(this.clickMe);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Il mio primo, bellissimo form!!!!!!!!!";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button clickMe;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label currentDateAndTime;
        private System.Windows.Forms.Label currentDateAndTime2;
        private System.Windows.Forms.TextBox person;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private MyNumericBox myNumericBox1;
        private MyComposedControl birthDay;
        private MyComposedControl myComposedControl2;
    }
}

