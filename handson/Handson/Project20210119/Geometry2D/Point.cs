﻿namespace Pippo.Pluto {
    class Paperino { }
}

namespace Project20210119.Geometry2D
{
    class Point
    {
        private double internalX;

        public Point(double x, double y)
        {
            this.internalX = x;
            Y = y;
        }

        public double X
        {
            get
            {
                return internalX;
            }
        }

        public double Y { get; }
        // private double ____Y;

        // Y = y ==> ___Y = y;

        // var aa = point.Y ===> var aa = point.get{ return ___Y; }
    }
}
