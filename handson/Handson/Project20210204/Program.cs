﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Project20210204
{
    class Program
    {
        static Random Random = new Random();
        const int iterations = 10;

        static void Main(string[] args) {
            //SimpleConcurrentThreads();
            //ShowCurrentThreadInfo();
            //CounterWithRaceConditions();
            //MonitorSyncSample.Run();
            //TaskSample.Run();
            //TaskSample.RunWithAsyncAwait();
            RaceConditionsSample.Run();            
        }

        static void CounterWithRaceConditions() {
            var counter = new Counter();
            const int threadCount = 20;
            const int incrementsPerThread = 1000;

            var threads = new List<Thread>();

            for (int i = 0; i < threadCount; i++) {
                var t = new Thread(() => {
                    //Thread.Sleep(Random.Next(150));
                    for (int j = 0; j < incrementsPerThread; j++) {
                        counter.Increment(Random.Next(2));
                        //Thread.Sleep(Random.Next(125));
                    }
                });
                threads.Add(t);
                t.Start();
            }
            var before = DateTimeOffset.Now;
            foreach(var t in threads) {
                t.Join();
            }
            var after = DateTimeOffset.Now;
            var elapsed = after - before;
            Console.WriteLine($"Value is {counter.Value}. Safe value is {counter.SafeValue}. Expected value was {incrementsPerThread * threadCount}. Elapsed {elapsed}");
        }

        static void ShowCurrentThreadInfo() {
            const int threadCount = 10;
            IList<Thread> threads = new List<Thread>();
            for (int i = 0; i < threadCount; i++)
            {
                var t = new Thread((n) =>
                {
                    Thread.Sleep(Random.Next(400));
                    Console.WriteLine($"{n} Name: {Thread.CurrentThread.Name}");
                    Console.WriteLine($"{n} Priority: {Thread.CurrentThread.Priority}");
                    Console.WriteLine($"{n} ThreadId: {Thread.CurrentThread.ManagedThreadId}");
                    Console.WriteLine($"{n} State: {Thread.CurrentThread.ThreadState}");
                });
                t.Priority = PriorityFor(i);
                t.Name = $"MyThread{i}";
                threads.Add(t);
                t.Start(i);
            }
            Console.WriteLine("Waiting for completion");
            for (int i = 0; i < 4; i++) {
                foreach (var t in threads) {
                    Console.WriteLine($"*** {t.Name} --> {t.ThreadState}");
                }
                Thread.Sleep(400);
            }
            Thread.Sleep(800);
            Console.WriteLine("Main thread completed");
        }

        static ThreadPriority PriorityFor(int i) {
            int rest = i % 3;
            switch (rest) {
                case 0: return ThreadPriority.Highest;
                case 1: return ThreadPriority.Normal;
                default: return ThreadPriority.Lowest;
            }
        }

        static void SimpleConcurrentThreads()
        {
            var myFirstThread = new Thread(BuildThread("F"));
            var mySecondThread = new Thread(BuildThread("S"));

            myFirstThread.Start();
            mySecondThread.Start();

            DoLoop("M");

            Console.WriteLine("\nWaiting for completion");
            Thread.Sleep(3000);
            Console.WriteLine("\nMain thread completed");
        }

        static void DoLoop(string text) {
            for (int i = 0; i < iterations; i++)
            {
                Thread.Sleep(Random.Next(50));
                Console.Write($"{text} ");
            }
        }

        static ThreadStart BuildThread(string text) {
            return () =>
            {
                DoLoop(text);
            };
        }

        class Counter {
            private int value = 0;
            private int safeValue = 0;
            public int Value => value;
            public int SafeValue => safeValue;

            private object syncRoot = new object();


            /*
             * private object SyncRoot1 = new object();
             
              counter++;
            lock (SyncRoot1)
            {
                syncCounter++;
            }
             
             */

            public void Increment(int randomSleep) {
                Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                value++;
                Thread.Sleep(randomSleep);
                //lock (syncRoot)
                //{
                //    safeValue++;
                //}

                // var result = Value + 1;
                // Value = result;

                // value = 10
                // T0: Stack-T0 result = 11, Value = 10
                // T1: Stack-T1 result = 11, Value = 10
                // T1: Stack-T1 result = 11, Value = 11
                // T1 completed Value = 11
                // T0: Stack-T0 result = 11, Value = 11
                // T0: completed Value = 11
            }
        }
    }
}
