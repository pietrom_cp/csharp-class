﻿using System;
using System.Threading;

namespace Project20210204
{
    class RaceConditionsSample
    {
        const int threadCount = 20;
        const int incrementsPerThread = 1000;



        internal static void Run()
        {
            Counter counter = new Counter();
            for (int i = 0; i < threadCount; i++)
            {
                Thread t = new Thread(counter.Execute) { Name = $"Thread # {i}" };
                t.Start(incrementsPerThread);
            }
            Thread.Sleep(20000);
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine($"Expected {threadCount * incrementsPerThread} actual {counter.Value} actual sync {counter.SyncValue}");
        }
    }

    class Counter
    {
        private static Random random = new Random();
        private int counter;
        private int syncCounter;

        private object SyncRoot1 = new object();
        private object SyncRoot2 = new object();

        public Counter()
        {
            counter = 0;
        }

        public void Increment()
        {
            counter++;
            lock (SyncRoot1)
            {
                syncCounter++;
            }
            //lock (x)
            //{
            //    DoSomething();
            //}
            // is equivalent to
            //System.Object obj = (System.Object)x;
            //System.Threading.Monitor.Enter(obj);
            //try
            //{
            //    DoSomething();
            //}
            //finally
            //{
            //    System.Threading.Monitor.Exit(obj);
            //}
        }

        public int Value => counter;
        public int SyncValue => syncCounter;

        internal void Execute(object param)
        {
            int incrementsPerThread = (int)param;
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            for (int i = 0; i < incrementsPerThread; i++)
            {
                Increment();
                Thread.Sleep(random.Next(2));
            }
        }
    }
}
