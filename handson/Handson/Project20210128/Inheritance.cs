﻿using System;

namespace Project20210128
{
    static class Inheritance
    {
        public static void UseInheritance()
        {
            Mammal m = new Mammal
            {
                Nickname = "Fido"
            };
            Cat c0 = new Cat
            {
                Nickname = "Neve"
            };
            Mammal c1 = new Cat();
            Mammal european = new EuropeanCat()
            {
                Nickname = "Sofia"
            };
            Cat c2 = new EuropeanCat();
            Mammal m1 = c2;

            EuropeanCat ec = new EuropeanCat();

            //m.WriteSomething(); // compilation error
            c0.WriteSomething();
            //c0.WriteSomethingEuropean(); // compilation error
            //european.WriteSomething(); // compilation error
            ec.WriteSomething();
            ec.WriteSomethingEuropean();

            Console.WriteLine($"Type of m is {m.GetType().Name}");
            Console.WriteLine($"Type of c0 is {c0.GetType().Name}");
            Console.WriteLine($"Type of c1 is {c1.GetType().Name}");
            Console.WriteLine($"Type of european is {european.GetType().Name}");

            var r = new Rectangle(10.1d, 19.7d);
            var s = new Square(100);
            var c = new Circle(10);
            Console.WriteLine($"r area = {r.Area()} 2p = {r.Perimeter()}");
            Console.WriteLine($"s area = {s.Area()} 2p = {s.Perimeter()}");
            Console.WriteLine($"c area = {c.Area()} 2p = {c.Perimeter()}");

            var shapes = new Shape[] { r, s, c, new Rectangle(10, 5), new Square(1) };
            foreach (var shape in shapes)
            {
                PrintShape(shape);
            }

        }

        static void PrintShape(Shape s)
        {
            Console.WriteLine($"A Shape! Area = {s.Area()} 2p = {s.Perimeter()}");
        }
    }
    class Mammal : System.Object
    {
        public string Nickname { get; set; }
    }

    class Cat : Mammal
    {

        public void WriteSomething()
        {
            Console.WriteLine("Miaooooo!");
        }
    }

    class EuropeanCat : Cat
    {
        public void WriteSomethingEuropean()
        {
            Console.WriteLine("Miaooooo UE");
        }
    }

    class Bird { }

    //class Hybrid : Cat, Bird { } // compilation error

    abstract class Shape
    {
        public void DoSomething()
        {
            Console.WriteLine("Very interesting feature here ;-) !");
        }
        public abstract double Area();
        public abstract double Perimeter();
    }

    class Circle : Shape
    {
        public double Radius { get; }

        public Circle(double radius)
        {
            Radius = radius;
        }

        public override double Area() => Math.PI * Radius * Radius;
        public override double Perimeter()
        {
            DoSomething();
            return 2 * Math.PI * Radius;
        }
    }

    class Rectangle : Shape
    {
        public double Width { get; }
        public double Height { get; }

        public Rectangle(double w, double h)
        {
            Width = w;
            Height = h;
        }

        public override double Area() => Width * Height;

        public override double Perimeter() => 2 * (Width + Height);
    }

    class Square : Rectangle
    {
        public Square(double side) : base(side, side)
        {
        }

        public Square() : this(20.0)
        {
        }
    }
}
