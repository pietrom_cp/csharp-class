﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpClass.Parameters
{
    class Simple
    {
        public static int Factorial(int n)
        {
            int result = 1;
            for (int i = 2; i <= n; i++)
            {
                result *= i;
            }
            return result;
        }

        public static int Factorial2(int n)
        {
            int result = 1;
            for (; n >= 2; n--)
            {
                result *= n;
            }
            return result;
        }

        public static void DoSimpleTest()
        {
            int m = 8;
            Console.WriteLine(Factorial(m));
            Console.WriteLine(Factorial2(m));
            Console.WriteLine(m);
        }
    }
}
