﻿using System;

namespace CSharpClass
{
	class ThePerson
	{
		public string Name { get; set; }
		public ThePerson(string name)
		{
			Name = name;
		}
		static ThePerson()
		{
			Console.WriteLine("Before the first ThePerson!");
		}
	}
}
