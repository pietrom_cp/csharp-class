﻿using MyDate = System.DateTime;
using CustomDate = CSharpClass.Custom.DateTime;
using System;

namespace CSharpClass
{
    namespace Nested
    {
        namespace NestedTwice
        {
            class Foo
            {
            }
        }

        class Bar { }
    }

    class FooBar { }

    class Namespaces
    {
        public void Method()
        {
            MyDate md = MyDate.UtcNow;
            CustomDate cd0 = new CustomDate();
            CustomDate cd1 = new CSharpClass.Custom.DateTime(); // :-O
            DateTime dt0 = DateTime.UtcNow;
            DateTime dt1 = md; // :-O

            Nested.NestedTwice.Foo foo0 = new Nested.NestedTwice.Foo(); // Can specify partial namespace only
            CSharpClass.Nested.NestedTwice.Foo foo1 = new CSharpClass.Nested.NestedTwice.Foo(); // Can specify full namespace
        }
    }
}

namespace CSharpClass.Custom
{
    class DateTime { }
}

namespace CSharpClass.Counting
{
    class Counter
    {
        public int Value { get; private set; }

        public Counter() : this(0)
        {
        }

        public Counter(int initialValue)
        {
            Value = initialValue;
        }

        public void Increment()
        {
            Value++;
        }

        public void Decrement()
        {
            Value--;
        }
    }
}
