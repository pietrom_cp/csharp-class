﻿using System;

namespace CSharpClass.Delegates
{
    delegate decimal Operation(decimal x, decimal y);

    delegate decimal Operation2(decimal x, decimal y);

    interface TheOperation {
        decimal Calculate(decimal x, decimal y);
    }

    class SumOperation : TheOperation
    {
        public decimal Calculate(decimal x, decimal y) {
            return x + y;
        }
    }

    class DelegatesSample
    {
        static decimal Sum(decimal x, decimal y)
        {
            return x + y;
        }

        static decimal Product(decimal x, decimal y)
        {
            return x * y;
        }

        static void ExecuteOperation(Operation op)
        {
            decimal a = 19.19m;
            decimal b = 11.11m;
            decimal result = op(a, b);
            Console.WriteLine($"Result is {result}");
        }
        internal static void UseDelegates()
        {
            ExecuteOperation((aa, bb) => aa - bb);
            ExecuteOperation(delegate (decimal aa, decimal bb) { return aa / bb; });
            ExecuteOperation(Sum);
            ExecuteOperation(Product);
        }

        static void ExecuteOperationF(Func<decimal, decimal, decimal> op)
        {
            decimal a = 19.19m;
            decimal b = 11.11m;
            decimal result = op(a, b);
            Console.WriteLine($"Result is {result}");
        }

        //static void ExecuteOperation(Func<decimal, decimal, decimal> op)
        //{
        //    decimal a = 19.19m;
        //    decimal b = 11.11m;
        //    decimal result = op(a, b);
        //    Console.WriteLine($"Result is {result}");
        //}

       

        internal static void UseFunctions()
        {
            Func<decimal, decimal, decimal> max = (x, y) => Math.Max(x, y);
            //Operation2 max2 = (x, y) => Math.Max(x, y);
            //ExecuteOperation(max2);
            ExecuteOperationF(max);
        }
    }
}
