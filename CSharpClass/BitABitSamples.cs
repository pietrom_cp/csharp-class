﻿using System;

namespace CSharpClass
{
    class BitABitSamples
    {
        internal static void DoSamples()
        {
            int value = 25;
            int newValue = value << 2;
            Console.WriteLine($"{value} << 2 is {newValue}");
        }
    }
}
