﻿using System;
using System.Threading.Tasks;

namespace ParallelWorking.AwaitAsync
{
    class TaskSamples
    {
        internal static void Run()
        {
            RunSample().ContinueWith(t => { Console.WriteLine("RunSample completed"); });
            Console.WriteLine("Run completed");
            Console.ReadKey();
        }

        private static async Task RunSample()
        {
            string result = await GetStringAsync2();
            Console.WriteLine($"Result is {result}");
        }

        private static async Task<string> GetStringAsync()
        {
            return (await GetSubstring1Async()) + " " + (await GetSubstring2Async()) + " (await)";
        }

        private static async Task<string> GetStringAsync2()
        {
            Console.WriteLine("GetStringAsync2.0");
            string first = await GetSubstring1Async();
            Console.WriteLine("GetStringAsync2.1");
            string second = await GetSubstring2Async();
            Console.WriteLine("GetStringAsync2.2");
            string third = await GetSubstring3Async();
            Console.WriteLine("GetStringAsync2.3");
            return $"{first} {second} {third} (await 2)";
        }

        private static Task<string> GetSubstring1Async()
        {
            Console.WriteLine("GetSubstring1Async.Start");
            return Task.Delay(1250).ContinueWith(x => Task.FromResult("SUB1")).Unwrap();
        }

        private static Task<string> GetSubstring2Async()
        {
            Console.WriteLine("GetSubstring2Async.Start");
            return Task.FromResult("SUB2");
        }

        private static Task<string> GetSubstring3Async()
        {
            Console.WriteLine("GetSubstring3Async.Start");
            return Task.Delay(1750).ContinueWith(x => Task.FromResult("SUB3")).Unwrap();
        }
    }
}
