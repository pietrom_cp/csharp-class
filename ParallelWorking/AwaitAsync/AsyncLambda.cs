﻿using System;
using System.Threading.Tasks;

namespace ParallelWorking.AwaitAsync
{
    class AsyncLambda
    {
        internal static void Run() {
            RunAsync().Wait();
        }

        internal static async Task RunAsync() {
            int r0 = await CalculateAsync((x, y) => Task.FromResult(x + y));
            Console.WriteLine(r0);
            int r1 = await CalculateAsync(async (x, y) => await Task.FromResult(x * y));
            Console.WriteLine(r1);
            int r2 = await CalculateAsync(DelayedSum);
            Console.WriteLine(r2);
            int r3 = await CalculateAsync(async (a, b) => await DelayedSum(a, b));
            Console.WriteLine(r3);
            return;
        }

        //internal static async Task<int> CalculateAsync(Func<int, int, Task<int>> operation)
        //{
        //    int result = await operation(11, 19);
        //    return result;
        //}

        internal static  Task<int> CalculateAsync(Func<int, int, Task<int>> operation)
        {
            return  operation(11, 19);
        }

        internal static async Task<int> DelayedSum(int x, int y)
        {
            await Task.Delay(500);
            return x + y;
        }
    }
}
