﻿using System;

namespace ParallelWorking
{
    class Program
    {
        static void Main(string[] args)
        {
            Threading.ThreadingMain.Run();
            //Tasks.TaskSamples.Run();
            //AwaitAsync.TaskSamples.Run();
            //AwaitAsync.ChainingMultipleTasks.Run();
            //AwaitAsync.ChainingMultipleTasks.Run2();
            //AwaitAsync.HandlingExceptions.Run();
            //AwaitAsync.HandlingExceptions.Run2();
            //AwaitAsync.HandlingExceptions.Run3();
            // AwaitAsync.AsyncLambda.Run();
            // Console.WriteLine("Press any key to exit");
            // Console.ReadKey();
        }
    }
}
