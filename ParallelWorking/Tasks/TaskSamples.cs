﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelWorking.Tasks
{
    class TaskSamples
    {
        internal static void Run()
        {
            //RunSimpleTasks();
            //RunSimpleTasksWithResult();
            RunSampleWithDelayedTask();
            //Console.ReadKey();
        }

        private static void RunSimpleTasks() {
            Task t1 = Task.Run(CreateJob("AA"));
            Task t2 = Task.Run(CreateJob("BB"));
            Task t3 = Task.Run(CreateJob("CC"));
            Task t4 = Task.Run(CreateJob("DD"));

            Task tAll = Task.WhenAll(t1, t2, t3, t4);

            tAll.ContinueWith((x) => { Console.WriteLine($"All tasks completed in thread {Thread.CurrentThread.ManagedThreadId}"); });

            Console.WriteLine($"Main thread completed in thread {Thread.CurrentThread.ManagedThreadId}");

            //Thread.Sleep(5000);
            //Console.WriteLine($"Main thread completed (2) in thread {Thread.CurrentThread.ManagedThreadId}");
        }

        private static Action CreateJob(string jobName)
        {
            return () =>
            {
                Console.WriteLine($"Start job {jobName} in thread {Thread.CurrentThread.ManagedThreadId}");
                Thread.Sleep(200);
                Console.WriteLine($"Job {jobName} completed");
            };
        }

        private static void RunSimpleTasksWithResult()
        {
            Task<int> t1 = Task.Run<int>(CreateJobWithResult("WW"));
            Task<int> t2 = Task.Run<int>(CreateJobWithResult("XX"));
            Task<int> t3 = Task.Run(CreateJobWithResult("YY"));
            Task<int> t4 = Task.Run(CreateJobWithResult("ZZ"));

            Task<int[]> tAll = Task.WhenAll(t1, t2, t3, t4);

            tAll.ContinueWith((x) => {
                int sum = x.Result.Sum();
                Console.WriteLine($"All tasks completed in thread {Thread.CurrentThread.ManagedThreadId}");
                Console.WriteLine($"Partial results are {string.Join(",", x.Result.Select(y => y.ToString()))}; sum is {sum}");                
            });
            tAll.Wait();
            Console.WriteLine($"Main thread completed in thread {Thread.CurrentThread.ManagedThreadId}");
        }

        private static readonly Random random = new Random();

        private static Func<int> CreateJobWithResult(string jobName)
        {
            return () =>
            {
                Console.WriteLine($"Start job {jobName} in thread {Thread.CurrentThread.ManagedThreadId}");
                Thread.Sleep(200);
                Console.WriteLine($"Job {jobName} completed");
                return random.Next(100);
            };
        }

        private static void RunSampleWithDelayedTask()
        {
            GetStringAsync().ContinueWith(t => {
                Console.WriteLine($"Result is {t.Result}");
            });
            Console.WriteLine("GetStringAsync invoked");
        }

        private static Task<string> GetStringAsync()
        {
            //return Task.Delay(750).ContinueWith(x => Task.FromResult("Pietro"));
            return Task.Delay(750).ContinueWith(x => Task.FromResult("Pietro")).Unwrap();
        }
    }
}
