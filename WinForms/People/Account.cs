﻿using System;
using System.Collections.Generic;

namespace WinForms.People
{
    static class Accounts
    {
        public static IEnumerable<Account> List = new List<Account>() {
            new Account("pietrom", "Pietro", "Martinelli", new Date(1978, 3, 19)),
            new Account("eddym", "Eddy", "Merckx", new Date(1945, 6, 17), false),
            new Account("migueli", "Miguel", "Indurain", new Date(1964, 7, 16)),
            new Account("albertoc", "Alberto", "Contador", new Date(1982, 12, 6))
        };
    }
    class Account
    {
        public Account(string username, string first, string last, Date birthday) : this(username, first, last, birthday, true)
        { }

        public Account(string username, string first, string last, Date birthday, bool isActive)
        {
            Username = username;
            FirstName = first;
            LastName = last;
            BirthdayAsDate = birthday;
            IsActive = isActive;
            BirthdayAsDateTime = birthday;
        }

        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        //public DateTime BirthdayAsDateTime => BirthdayAsDate;
        public DateTime BirthdayAsDateTime { get; set; }
        public Date BirthdayAsDate { get; set; }
        public bool IsActive { get; set; }

        public override string ToString()
        {
            return $"{Username}:{FirstName}:{LastName}:{BirthdayAsDate}:{BirthdayAsDateTime}:{IsActive}";
        }
    }

    struct Date
    {
        public Date(int year, int month, int day)
        {
            Year = year;
            Month = month;
            Day = day;
        }
        private int Year { get; }
        private int Month { get; }
        private int Day { get; }

        public static Date From(string s)
        {
            string[] fields = s.Split('/');
            int year = int.Parse(fields[2]);
            int month = int.Parse(fields[1]);
            int day = int.Parse(fields[0]);
            return new Date(year, month, day);
        }

        public static implicit operator Date(DateTime dateTime)
        {
            return new Date(dateTime.Year, dateTime.Month, dateTime.Day);
        }

        public static implicit operator DateTime(Date date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Unspecified);
        }

        public static implicit operator string(Date date) {
            return date.ToString();
        }

        public static implicit operator Date(string s)
        {
            return Date.From(s);
        }

        public override string ToString()
        {
            return $"!!!!{Day.ToString("00")}/{Month.ToString("00")}/{Year.ToString("0000")}";
        }
    }
}
